package rmq.entregables.sprintbootpractitioner.service;

import org.springframework.stereotype.Service;
import rmq.entregables.sprintbootpractitioner.model.ProductoModel;

public interface ProductoService {

    public ProductoModel getProducto(int id);

    public ProductoModel addProducto(ProductoModel productoModel);

    public void updateProducto(int id, ProductoModel productToUpdate);

    public void removeProducto(int id);

    public int getIndex(int index);
}
