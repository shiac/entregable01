package rmq.entregables.sprintbootpractitioner.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rmq.entregables.sprintbootpractitioner.model.ProductoModel;
import rmq.entregables.sprintbootpractitioner.model.UserModel;
import rmq.entregables.sprintbootpractitioner.service.ProductoService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductoServiceImpl implements ProductoService {

    private List<ProductoModel> productoModelList;

    public ProductoServiceImpl() {
        productoModelList = new ArrayList<ProductoModel>();
        productoModelList.add(new ProductoModel(1, "producto 1", 100.00,10));
        productoModelList.add(new ProductoModel(2, "producto 2", 200.00,10));
        productoModelList.add(new ProductoModel(3, "producto 3", 300.00,10));
        productoModelList.add(new ProductoModel(4, "producto 4", 400.00,10));
        productoModelList.add(new ProductoModel(5, "producto 5", 500.00,10));
        List<UserModel> users = new ArrayList<>();
        users.add(new UserModel("1"));
        productoModelList.get(0).setUsers(users);
        users.add(new UserModel("2"));
        productoModelList.get(1).setUsers(users);
        users.add(new UserModel("3"));
        users = new ArrayList<>();
        users.add(new UserModel("4"));
        productoModelList.get(2).setUsers(users);
        users.add(new UserModel("5"));
        productoModelList.get(3).setUsers(users);
        users = new ArrayList<>();
        productoModelList.get(4).setUsers(users);
        users.add(new UserModel("6"));
    }

    @Override
    public ProductoModel getProducto(int id){
        if(getIndex(id)>=0) {
            return productoModelList.get(getIndex(id));
        }
        return null;
    }

    @Override
    public ProductoModel addProducto(ProductoModel productoModel) {
        productoModelList.add(productoModel);
        return productoModel;
    }

    @Override
    public void updateProducto(int id, ProductoModel productToUpdate){
        ProductoModel productoModel =productoModelList.get(getIndex(id));
        removeProducto(id);
        productoModel.setNombre(productToUpdate.getNombre());
        productoModel.setCantidad(productToUpdate.getCantidad());
        productoModel.setPrecio(productToUpdate.getPrecio());
        productoModelList.add(productoModel);
    }

    @Override
    public void removeProducto(int id){
        productoModelList.remove(getIndex(id));
    }

    @Override
    public int getIndex(int index) {
        int i=0;
        while(i<productoModelList.size()) {
            if(productoModelList.get(i).getId() == index){
                return(i);
            }
            i++;
        }
        return -1;
    }
}
