package rmq.entregables.sprintbootpractitioner.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import java.util.List;
import java.util.Objects;

public class ProductoModel {
    @JsonIgnore
    @Id private int id;
    private String nombre;
    private double precio;
    private int cantidad;
    private List<UserModel> users;

    public ProductoModel() {
    }

    public ProductoModel(int id, String descripcion, double precio,int cantidad) {
        this.id = id;
        this.nombre = descripcion;
        this.precio = precio;
        this.cantidad=cantidad;
    }


    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getCantidad() {
        return cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public List<UserModel> getUsers(){
        return this.users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductoModel{");
        sb.append("id=").append(id);
        sb.append(", nombre='").append(nombre).append('\'');
        sb.append(", precio=").append(precio);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductoModel)) return false;
        ProductoModel productoModel = (ProductoModel) o;
        return getId() == productoModel.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
