package rmq.entregables.sprintbootpractitioner.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rmq.entregables.sprintbootpractitioner.model.ProductoModel;
import rmq.entregables.sprintbootpractitioner.model.ProductoPrecioOnly;
import rmq.entregables.sprintbootpractitioner.service.ProductoService;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoService productService;

    @GetMapping("")
    public String root() {
        return "RMQ Techu API REST v1.0.0";
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id) {
        ProductoModel productoModel = productService.getProducto(id);
        if (productoModel == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(productoModel);
    }

    @PostMapping("/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel) {
        productService.addProducto(productoModel);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity updateProducto(@PathVariable int id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel productoModel = productService.getProducto(id);
        if (productoModel == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.updateProducto(id, productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity deleteProducto(@PathVariable int id) {
        ProductoModel productoModel = productService.getProducto(id);
        if(productoModel == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productService.removeProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody ProductoPrecioOnly productoPrecioOnly, @PathVariable int id){
        ProductoModel productoModel = productService.getProducto(id);
        if (productoModel == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoModel.setPrecio(productoPrecioOnly.getPrecio());
        productService.updateProducto(id, productoModel);
        return new ResponseEntity<>(productoModel, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable int id){
        ProductoModel productoModel = productService.getProducto(id);
        if (productoModel == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (productoModel.getUsers()!=null)
            return ResponseEntity.ok(productoModel.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
